#include <stdio.h>

int main(void) {

  int c, t, b, s;

  t = b = s = 0;
  
  while((c = getchar()) != EOF){
	if (c == '\t') {
	  ++t;
	}
	if (c == '\b') {
	  ++b;
	}
	if (c == '\\') {
	  ++s;
	}
	printf("%d, %d, %d\n", t, b, s);
  }
  
  return 0;
}
