#include <stdio.h>

int main(void) {

  int c, counter, i, j;
  int length[10];
  
  for (i = 0; i < 10; ++i)
	length[i] = 0;

  counter = 0;
  while ((c = getchar()) != EOF) {
	if (c == ' ' || c == '\t' || c == '\n') {
	  ++length[counter];
	  counter = 0;
	}
	else
	  ++counter;
  }
  for (i = 0; i < 10; ++i) {
	printf("%d: ", i);
	for (j = 0; j < length[i]; ++j) {
	  printf("#");
	}
	printf("\n");
  }

  return 0;
}
