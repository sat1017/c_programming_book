#include <stdio.h>

float ftoc(float f);

int main(void){
  
  float fahr;
  int lower, upper, step;

  lower = 0;
  upper = 300;
  step = 20;

  fahr = lower;
  printf("Fahr Celsius\n");
  while (fahr <= upper) {
	printf("%3.0f %6.1f\n", fahr, ftoc(fahr));
	fahr = fahr + step;
  }
  
  return 0;
}

float ftoc(float f) {
  float c;
  c = (5.0/9.0) * (f-32.0);
  return c;
}
