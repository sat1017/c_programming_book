#include <stdio.h>
#define MAXLINE 1000 /*  maximum input line size */
#define LONGLINE 80

int getLine(char line[], int maxline);
void copy(char to[], char from[], int end);
int removeLine(char line[]);

/*  print longest input line */
int main(void)
{
  int len; /*  current line length */
  int max; /*  maximum length seen so far */
  char line[MAXLINE]; /*  current input line */
  char longest[MAXLINE]; /*  longest line saved here */

  max = 0;
  while (len = getLine(line, MAXLINE) > 0)
	if (removeLine(line) > 0)
	  printf("%s\n",line);
  return 0;
}


/*  getline: read a line into s, return length */
int getLine(char s[], int lim)
{
  int c, i;

  for (i=0; i<lim-1 && (c=getchar()) != EOF && c!='\n'; ++i)
	s[i] = c;
  if (c == '\n') {
	s[i] = c;
	++i;
  }
  s[i] = '\0';
  return i;
}


/*  remove trailing blanks and tabs */
int removeLine(char s[])
{
  /*  1) index through array and find length via newline character
	  2) step backwords until find first character
	  3) step forward and add newline
	  4) step forward and add EOF
	  5) check if line is blank (0 len)
	  6) return length
  */

  int i;
  /* char new_line[MAXLINE]; /\* new line *\/ */
  
  /* find end of string */
  while (s[i] != '\n')
	++i;
  --i; /* the end of actual characters */
  
  /* find the end of the trailing spaces or tabs  */
  while (s[i] == ' ' || s[i] == '\t')
	--i;
  
  /* copy old array to a new array w/o any trailing blanks*/
  /* copy(new_line, s, i); */

  /* add EOF and newline characters */
  ++i;
  s[i] = '\n';
  ++i;
  s[i] = '\0';
  /* printf("%s\n", new_line); */
  return i;
}

/*  copy: copy from into to */
void copy(char to[], char from[], int end)
{
  int i;
  
  i = 0;
  while ((to[i] = from[i]) != '\n' && i <= end)
	++i;
}

