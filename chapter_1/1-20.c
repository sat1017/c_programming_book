#include <stdio.h>

#define TABSTOP 8 /* tab inc size */

/* replace tabs with number of spaces */
int main(void)
{
  /*
	1) define how many spaces = tab
	2) getchar()
	3) if '\t'
	4) shift input with putchar() by number of spaces
	5) insert spaces into new array locations
	6) remove '\t'
*/

  int c; /* new char */
  int i; /* counter for number of spaces */

  i = 1;
  while ((c = getchar()) != EOF) {
	if (c == '\t') {
	  /* if remainder != 0 need to add space */
	  while ((TABSTOP - (i - 1)) % TABSTOP) {
		putchar(' ');
		++i;
	  }
	} else if (c == '\n') {/* if new line, print and reset counter */
	  putchar(c);
	    i = 1;
	} else { /* else print char and increment counter */
	    putchar(c);
	    ++i;
	}
  }
  return 0;
}
