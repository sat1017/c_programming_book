#include <stdio.h>

void rcomment(int c);

/* remove comments from code */
int main(void)
{

  int c;
  
  while ((c=getchar()) != EOF)
	rcomment(c);
  return 0;
}

void rcomment(int c)
{
  int d;
  
  if (c == '/') {
	if ((d=getchar()) == '*') { 	/* begining of comment  */
	  c = getchar();
	  d = getchar();
	  while (c != '*' && d != '/') { /* check if you are at the end of a comment */
		c = d;
		d = getchar();
	  }
	}
  } else
	putchar(c);
}
