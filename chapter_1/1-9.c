#include <stdio.h>

#define SINGLE_BLANK 0
#define MULTI_BLANK 1

int main(void) {

  int c, state;

  state = SINGLE_BLANK;
  
  while((c = getchar()) != EOF) {
	if (c == ' ') {
	  if (state == SINGLE_BLANK) {
		putchar(c);
		state = MULTI_BLANK;
	  }
	} else {
	  putchar(c);
	  state = SINGLE_BLANK;
	}
  }
  
  return 0;
  
}
