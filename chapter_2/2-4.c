#include <stdio.h>

void squeeze(char s1[], char s2[]);

int main(void)
{
  char s1[] = "abccfd";
  char s2[] = "cde";
  
  printf("%s\n", s1);
  squeeze(s1, s2);
  printf("%s\n", s1);
  
  return 0;
}
/* delete any char in s1 if char is in s2 */
void squeeze(char s1[], char s2[])
{
  /* 1) compare s1 char to every char in s2
	 2) if s1 == s2 replace s1 index with next char
*/
  int i; /* s1 index */
  int j; /* s2 index */
  int k; /* s1 index for what you want to keep  */

  for (i = k = 0; s1[i] != '\0'; i++) {
	for (j = 0; s2[j] != '\0' && s1[i] != s2[j]; j++)
	  ;
	if (s2[j] == '\0')
	  s1[k++] = s1[i];
  }
  s1[k] = '\0';
  
  return;
}


