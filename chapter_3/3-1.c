#include <stdio.h>
#include <limits.h>

/* if I use INT_MAX or anything above 1m I get seg fault which is ~2b */
#define MAXLEN 1000000
int binsearch(int x, int v[], int n);

int main(void) {
  int x;
  int n;
  int i;
  int k;

  n = MAXLEN;
  int v[n];
  k = 0;
  
  for (i = 0; i < n; i++)
	v[i] = k++;
  
  x = 67;
  printf("%d\n", binsearch(x, v, n));
  
  return 0;
}


int binsearch(int x, int v[], int n)
{
  int low, high, mid;

  low = 0;
  high = n - 1;
  mid = (low+high) / 2;
  
  while (low <= high && x != v[mid]) {
	if (x < v[mid])
	  high = mid - 1;
	else    /* found match */
	  low = mid + 1;
	mid = (low+high) / 2;
  }
  
  if(x == v[mid])
	return mid;
  else
	return -1; /* no match */
}
