#include <stdio.h>

#define MAXLEN 1000

void escape(char s[], char t[]);
  
int main(void) {
  
  char s[MAXLEN] = "I am a really\t long\nstring\nwith a bunch\tof tabs and newlines";
  char t[MAXLEN];
  
  escape(s, t);
  printf("%s\n%s\n", s, t);
  
  return 0;
}

/* copies array s to t and converts newline and tab characters into visable \t and \n */
void escape(char s[], char t[])
{
  int i; /* s index */
  int j; /* t index */
  
  for (i = 0, j = 0; s[i] != '\0'; i++) {
	switch (s[i]) {
	case '\n':
	  t[j++] = '\\';
	  t[j++] = 'n';
	  break;
	case '\t':
	  t[j++] = '\\';
	  t[j++] = 't';
	  break;
	default:
	  t[j++] = s[i];
	}
  }
  
  return;
}
