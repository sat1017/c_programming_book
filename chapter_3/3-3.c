#include <stdio.h>

#define MAXLEN 1000

void expand(char s1[], char s2[]);
  
int main(void) {
  
  char s1[MAXLEN] = "-a-c-z0-2-9-";
  char s2[MAXLEN];
  
  expand(s1, s2);
  printf("%s: %s\n", s1, s2);
  
  return 0;
}

/* expands shorthand notation like a-z in string s1 into the equivalent complete list abc..xyz in s2
   allow letters of either case and digits
   allow multiple hypen sequences like a-b-c or a-z0-9 or -a-z
   allow leading or trailing hypen to be taken literally
  */
void expand(char s1[], char s2[])
{
  int i; /* s1 index */
  int j; /* s2 index */
  int c; /* current char */

  j = 0;
  for (i = 0; (c = s1[i]) != '\0'; i++) {
	if (s1[i+1] == '-' && s1[i+2] >= c) {
	  while (c <= s1[i+2])
		s2[j++] = c++;
	  }
	else
	  s2[j] = c;
  }
  s2[j] = '\0';
}
