#include <stdio.h>

#define MAXLEN 1000

void itob(int n, int b, char s[]);
void reverse(char s[]);
  
int main(void) {
  
  char s[MAXLEN] = "511";
  
  itob(4564, 2, s);
  printf("%s\n", s);
  
  return 0;
}

/* converts the integers in n into a base b character representation in the string s
  */
void itob(int n, int b, char s[])
{
  int i, j,
	sign;

  if ((sign = n) < 0)			/* record sign */
	n = -n;						/* make n positive */
  i = 0;
  do {							/* generate digits in reverse order */
	j = n % b;
	s[i++] = (j <= 9) ? j + '0' : j + 'a' - 10;		/* get next digit */
  } while ((n /= b) > 0);		/* delete it */
  if (sign < 0)
	s[i++] = '-';
  s[i] = '\0';
  reverse(s);
}


/* reverse: reverse the input one line at a time */
void reverse(char s[])
{

  int i; /* original line counter */
  int j; /* reversed line counter */
  char tmp; /* tmp swapped char */

  /* len of original string */
  i = 0;
  while (s[i] != '\0')
	++i;
  --i;
  if (s[i] == '\n')
	--i;

  j = 0;
  /* once i = j you have met at the middle and swaped everything */
  while (j < i) {
	tmp = s[j];
	s[j] = s[i];
	s[i] = tmp;
	--i;
	++j;
  }
}
