#include <stdio.h>
#include <string.h>

#define MAXLINE 1000

int getLine(char line[], int max);
int strindex(char source[], char searchfor[]);
int strrindex(char s[], char t[]);

char pattern[] = "ould";

/* find last position in s that contains t*/
int main(void)
{
  char line[MAXLINE];
  int found = 0;
  int pos;

  while (getLine(line, MAXLINE) > 0)
	if ((pos = strrindex(line, pattern)) >= 0) {
	  printf("%d\n", pos);
	  found++;
	}
  return found;
}

/*  getline: get line into s, return length */
int getLine(char s[], int lim)
{
  int c, i;

  i = 0;
  while (--lim > 0 && (c=getchar()) != EOF && c != '\n')
	s[i++] = c;
  if (c == '\n')
	s[i++] = c;
  s[i] = '\0';
  return i;
}

/* strindex: return index of t in s, -1 if none */
int strindex(char s[], char t[])
{
  int i, j, k;

  for (i = 0; s[i] != '\0'; i++) {
	for (j=i, k=0; t[k]!='\0' && s[j]==t[k]; j++, k++)
	  ;
	if (k > 0 && t[k] == '\0')
	  return i;
  }
  return -1;
}

/* strrindex: return last position in s where t occurs */
int strrindex(char s[], char t[])
{
  int i, j, k;

  for (i = strlen(s) - strlen(t); i >= 0; i--) {
	for (j=i, k=0; t[k]!='\0' && s[j]==t[k]; j++, k++)
	  ;
	if (k > 0 && t[k] == '\0')
	  return i;
  }
  return -1; 
}
