#include <stdio.h>

#define MAXLEN 1000

void itoa(int n, char s[]);
void reverse(char s[]);
  
int main(void) {
  
  char s[MAXLEN];
  int n = -1234;
  
  itoa(n, s);
  printf("%s\n", s);
  
  return 0;
}

/* converts the integers in n into a base b character representation in the string s
  */
void itoa(int n, char s[])
{
  static int i;
  
  if (n < 0){
	s[i++] = '-';
	itoa(-n, s);
  } else if (n < 10)
	s[i++] = n + '0';
  else {
	itoa(n/10, s);
	s[i++] = n % 10 + '0';
  }
  s[i] = '\0';
}
