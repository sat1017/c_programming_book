#include <stdio.h>
#define MAXLINE 1000 /*  maximum input line size */
#define LONGLINE 80

int getLine(char s[], int maxline);
void reverse(char s[]);

/*  print reverse input line */
int main(void)
{
  char line[MAXLINE]; /*  current input line */

  while (getLine(line, MAXLINE) > 0) {
	reverse(line);
	printf("%s", line);
  }
  return 0;
}


/*  getline: read a line into s, return length */
int getLine(char s[], int lim)
{
  int c, i, j;
  j = 0;
  for (i=0; (c=getchar()) != EOF && c!='\n'; ++i)
	if (i < lim-2) {
	  s[j] = c;
	  ++j;
	}
  if (c == '\n') {
	s[j] = c;
	++i;
	++j;
  }
  s[j] = '\0';
  return i;
}


/* reverse: reverse the input one line at a time */
void reverse(char s[])
{
  /* 1) find len of original array less EOF and newline
	 2) create new empty array of same len 
	 3) take last index of original array  less EOF and newlone and place it in first index of new array
	 4) decrement old counter; increment new counter
	 5) repeat 3-4 until old counter == new counter
   */

  int i; /* original line counter */
  int j; /* reversed line counter */
  char tmp; /* tmp swapped char */

  /* len of original string */
  i = 0;
  while (s[i] != '\0')
	++i;
  --i;
  if (s[i] == '\n')
	--i;

  j = 0;
  /* once i = j you have met at the middle and swaped everything */
  while (j < i) {
	tmp = s[j];
	s[j] = s[i];
	s[i] = tmp;
	--i;
	++j;
  }
}
