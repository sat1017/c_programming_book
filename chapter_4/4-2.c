#include <stdio.h>
#include <string.h>

#define MAXLEN 100

double atof(char s[]);

int main(void)
{
  char s[MAXLEN] = "123.456e2";
  printf("%f\n", atof(s));
  return 0;
}


double atof(char s[])
{
  double val, power;
  int i, sign, esign, epower;

  printf("%ld\n", strlen(s));	/* sanity check for len of string */
 
  for (i=0; isspace(s[i]); i++)	/* skip white spaces */
	;
  sign = (s[i] == '-') ? -1 : 1;
  if (s[i] == '+' || s[i] == '-')
	i++;
  for (val = 0.0; isdigit(s[i]); i++)
	val = 10.0 * val + (s[i] - '0');
  if (s[i] == '.')
	i++;
  for (power = 1.0; isdigit(s[i]); i++) {
	val = 10.0 * val + (s[i] - '0');
	power *= 10.0;
  }
  if (s[i] == 'e' || s[i] == 'E')
	i++;
  if (s[i] == '-') {			/* check sign of exponent */
	esign = -1;
	i++; 						/* need to increment due to negative sign */
  }
  else
	esign = 1; 
  for (epower = 0; isdigit(s[i]); i++) {
	epower = 10 * epower + (s[i] - '0');
  }
  while (epower > 0) {
	if (esign == -1) {
	  power *= 10.0;
	} else if (esign == 1) {
	  power /= 10.0;
	}
	epower--;
  }

  return sign * val / power;
}
