#include <stdio.h>
#include <stdlib.h>				    /* for atof() */
#include <ctype.h>
#include <string.h>
#include <math.h>				    /* for fmod() */

#define MAXOP 100				    /* max size of operand or operator */
#define NUMBER '0'				    /* signal that a number was found */
#define MAXVAL 100				    /* maximum depth of val stack */
#define BUFSIZE 100
#define FUNCNAME 'n'				/* signal that a math function was found */
#define ANSWER 'a'					/* signal that the previous answer was found */

int sp = 0;						    /* next free stack position */
double val[MAXVAL];				    /* value stack */
char buf[BUFSIZE];			 	    /* buffer for ungetch */
int bufp = 0;					    /* next free position in buf */

int getop(char []);
void push(double);
double pop(void);
int getch(void);
void ungetch(int);
void clear(void);
void showTop(void);
void swap(void);
void duplicate(void);
void showStack(void);
void dealWithFunc(char s[]);
void ungets(char s[]);


/* getch: get a (possibly pushed back) character */
int getch(void)
{
  return (bufp > 0) ? buf[--bufp] : getchar();
}


/* ungets: push back an entire string onto the input buffer */
void ungets(char s[])
{
  for (int i = strlen(s); i > 0; i--)
	ungetch(s[i]);
}


/* ungetch: push character back on input */
void ungetch(int c)
{
  if (bufp >= BUFSIZE)
	printf("ungetch: too many characters\n");
  else
	buf[bufp++] = c;
}


/* push: push f onto value stack */
void push(double f)
{
  if (sp < MAXVAL)
	val[sp++] = f;
  else
	printf("error: stack full, can't push %g\n", f);
}


/* pop: pop and return top value from stack */
double pop(void)
{
  if(sp > 0)
	return val[--sp];
  else {
	printf("error: stack empty\n");
	return 0.0;
  }
}


/* clear: clear the stack by resetting the stack pointer*/
void clear(void)
{
  sp = 0;
}

/* showTop: display the top without poping the stack */
void showTop(void)
{
  if (sp > 0)
	printf("top of the stack contains: %.8g\n", val[sp-1]);
  else
	printf("the stack is empty\n");
  
}


/* duplicate: duplicate the top element */
void duplicate(void)
{
  double tmp = pop();
  push(tmp);
  push(tmp);
}


/* swap: swap the top two elements of the stack */
void swap(void)
{
  double tmp1 = pop();
  double tmp2 = pop();
  push(tmp1);
  push(tmp2);
}


/* showStack: helper function to visualize the stack */
void showStack(void)
{
  for (int i =0; i <= sp; i++)
	printf("%g ", val[i]);
  printf(": %d\n", sp);
}


/* dealWithFunc: determines which math function is called and performs the operation */
void dealWithFunc(char s[])
{
  double op2;
  
  if (strcmp(s, "sin") == 0)
	push(sin(pop()));
  else if (strcmp(s, "exp") == 0)
	push(exp(pop()));
  else if (strcmp(s, "pow") == 0) {
	op2 = pop();
	push(pow(pop(), op2));
  } else
	printf("error: %s not supported\n", s);
}


/* getop: get next operator or numeric operand */
int getop(char s[])
{
  int i, c;

  while ((s[0] = c = getch()) == ' ' || c == '\t') /* skips spaces and tabs and recalls getchar */
	;
  s[1] = '\0';
  
  i = 0;
  if (islower(c)) {
	while (islower(s[++i] = c = getch()))
	  ;
	s[i] = '\0';
	if (c != EOF)
	  ungetch(c);
	if (strlen(s) > 1)			
	  return FUNCNAME;			/* math function */
	else
	  return c;					/* variable */
  }
  
  if (!isdigit(c) && c != '.' && c != '-')
	return c;					/* not a number; return the math operator, stack operator, or variable assignment */
  
  if (c == '-')
	if (isdigit(c = getch()) || c == '.')
	  s[++i] = c;				/* negative number */
	else {
	  if (c != EOF)
		ungetch(c);
	  return '-';				/* operator */
	}
  if (isdigit(c))				/* collect integer part */
	while (isdigit(s[++i] = c = getch()))
	  ;
  if (c == '.')					/* collect fraction part */
	while (isdigit(s[++i] = c = getch()))
	  ;
  s[i] = '\0';
  if (c != EOF)
	ungetch(c);
  return NUMBER;
}


int main(void)
{
  int type, i , var;
  double op2, v;
  char s[MAXOP];
  double variable[26];				/* 26 variable array a = 0, b = 1, etc. */

  for (i = 0; i < 26; i++)	/* initialize the variable array with all zeros */
	variable[i] = 0.0;

  while ((type = getop(s)) != EOF) {
	showStack();
	switch (type) {
	case NUMBER:
	  push(atof(s));
	  break;
	case FUNCNAME:				/* deal with math functions */
	  dealWithFunc(s);
	  break;
	case '+':
	  push(pop() + pop());
	  break;
	case '*':
	  push(pop() * pop());
	  break;
	case '-':
	  op2 = pop();
	  push(pop() - op2);
	  break;
	case '/':
	  op2 = pop();
	  if (op2 != 0.0)
		push(pop() / op2);
	  else
		printf("error: zero divisor\n");
	  break;
	case '%':
	  op2 = pop();
	  if (op2 != 0.0)
		push(fmod(pop(), op2));
	  else
		printf("error: zero divisor\n");
	  break;
	case'?': 					/* print top element of the stack */
	  showTop();
	  break;
	case '!':					/* clear the stack */
	  clear();
	  break;
	case '@': 					/* duplicate top element of the stack */
	  duplicate();
	  break;
	case '$':					/* swap the top two elements */
	  swap();
	  break;
	case '=':					/* variable assignment */
	  pop();
	  if (var >= 'A' && var <= 'Z')
		variable[var - 'A'] = pop();
	  else
		printf("error: no variable name\n");
	  break;
	case '\n':
	  v = pop();
	  printf("\t%.8g\n", v);
	  break;
	default:
	  if (type >= 'A' && type <= 'Z') 
		push(variable[type - 'A']);
	  else if (type == 'v')
		push(v);
	  else
		printf("error: unknown command %s\n", s);
	  break;
	}
	var = type;
  }
  return 0;
}
