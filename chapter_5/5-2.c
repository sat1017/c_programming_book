#include <stdio.h>
#include <ctype.h>

#define BUFSIZE 100

char buf[BUFSIZE];				/* buffer size */
int bufp = 0;					/* next free position in buf */

int getch(void);
void ungetch(int);


/* getch: get a (possibly pushed back) character */
int getch(void)
{
  return (bufp > 0) ? buf[--bufp] : getchar();
}


/* ungetch: push character back on input */
void ungetch(int c)
{
  if (bufp >= BUFSIZE)
	printf("ungetch: too many characters\n");
  else
	buf[bufp++] = c;
}


/* getint: get next integer from input into *pn */
int getfloat(float *pn)
{
  int c, sign;
  double val, power;

  while (isspace(c=getch()))
	;
  if (!isdigit(c) && c != EOF && c != '-' && c != '+' && c != '.') {
	ungetch(c);
	return 0;
  }
  sign = (c == '-') ? -1 : 1;
  if (c == '-' || c == '+')
	c = getch();
  for (val = 0.0; isdigit(c); c = getch()) {
	val = 10.0 * val + (c - '0');
  }
  if (c == '.')
	c = getch();
  for (power = 1.0; isdigit(c); c = getch()) {
	val = 10.0 * val + (c - '0');
	power *= 10.0;
  }
  *pn = sign * val / power;
  if (c != EOF) {
	ungetch(c);
  }
  return c;
}

int main(void)
{
  int n;
  float array[BUFSIZE];

  for (n = 0; n < BUFSIZE && getfloat(&array[n]) != EOF; n++) { 
	printf("array[%d] = %f\n", n, array[n]);
  }
  return 0;
}
