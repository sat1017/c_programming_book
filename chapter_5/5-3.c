#include <stdio.h>

#define BUFFER 100

void strCat(char *s, char *t);


/* strcat: concatenate t to the end of s; s must be big enough */
void strCat(char *s, char *t)
{
  while (*s++ != '\0')
	;
  --s;
  while ((*s++ = *t++) != '\0')
	;
}


int main(void)
{
  char a[BUFFER] = "I am a string";
  char b[BUFFER] = "that is really long";

  strCat(a, b);
  printf("%s\n", a);

  return 0;
}
