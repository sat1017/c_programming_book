#include <stdio.h>

#define BUFFER 100

int strend(char *s, char *t);


/* strend: returns 1 if the string t occurs at the end of s, and 0 otherwise */
int strend(char *s, char *t)
{
  char *ps = s;
  char *pt = t;

  for ( ; *s; s++)				/* end of s */
	printf("%c ", *s);
  for ( ; *t; t++)				/* end of t */
	;
  for ( ; *s == *t; s--, t--) {
	if (s == ps || t == pt)
	  return 1;
  }
  return 0;
  
}


int main(void)
{
  char a[BUFFER] = "I am a string";
  char b[BUFFER] = "xng";

  printf("%d\n",strend(a, b));

  return 0;
}
