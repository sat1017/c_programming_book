#include <stdio.h>
#include <string.h>

#define MAXLINES 5000			/* max lines to be sorted */
#define MAXSTOR 10000			/* size of available space */
#define MAXLEN 1000				/* max length of any input lines */


char *lineptr[MAXLINES];		/* pointers to text lines */
char linestor[MAXSTOR];

int readlines(char *lineptr[], int nlines, char *linestor);
void writelines(char *lineptr[], int nlines);
void qsort(char *lineptr[], int left, int right);
int getLine(char *, int);
char *alloc(int);


/* sort input lines */
int main(void)
{
  int nlines;					/* number of input lines read */

  if ((nlines = readlines(lineptr, MAXLINES, linestor)) >= 0) {
	qsort(lineptr, 0, nlines-1);
	writelines(lineptr, nlines);
	return 0;
  } else {
	printf("error: input too big to sort\n");
	return 1;
  }
}


/* readlines: read input lines */
int readlines(char *lineptr[], int maxlines, char *linestor)
{
  int len, nlines;
  char *p = linestor;
  char line[MAXLEN];
  char *linestop = linestor + MAXSTOR; /* pointer to the address at the end of the array */
  
  nlines = 0;
  while ((len = getLine(line, MAXLEN)) > 0)
	if (nlines >= maxlines || p+len > linestop)
	  return -1;
	else {
	  line[len-1] = '\0';		/* delete newline and replace with Null char*/
	  strcpy(p, line);
	  lineptr[nlines++] = p;
	  p += len;					/* point to the next free address in linestor */
	}
  return nlines;
}


/* writelines: write output lines */
void writelines(char *lineptr[], int nlines)
{
  int i;

  for (i = 0; i < nlines; i++)
	printf("%s\n", lineptr[i]);
}


int getLine(char s[], int lim)
{
  int c, i;

  for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; i++)
    s[i] = c;
  if (c == '\n') {
    s[i++] = c;
  }
  s[i] = '\0';
  return i;
}


/* qsort: sort v[left]...v[right] into increasing order */
void qsort(char *v[], int left, int right)
{
  int i, last;
  void swap(char *v[], int i, int j);

  if (left >= right)			/* do nothing if array contains */
	return;						/* fewer than two elements */
  swap(v, left, (left+right)/2);
  last = left;
  for (i = left+1; i <= right; i++)
	if (strcmp(v[i], v[left]) < 0)
	  swap(v, ++last, i);
  swap(v, left, last);
  qsort(v, left, last-1);
  qsort(v, last+1, right);
}


/* swap: interchange v[i] and v[j] */
void swap(char *v[], int i, int j)
{
  char *temp;

  temp = v[i];
  v[i] = v[j];
  v[j] = temp;
}


void afree(char *p)
{
  if (p >= allocbuf && p < allocbuf + ALLOCSIZE)
	allocp = p;
  
}
