#include <stdio.h>
#include <ctype.h>
#include <string.h>

#define MAXWORD 100
#define NKEYS (sizeof keytab / sizeof(struct key))

char buf = 0;			 	        /* buffer for ungetch */


struct key {

	char *word;
	int count;
} keytab[] = {};

int getword(char *word, int lim);
int binsearch(char *, struct key *, int);

int main()
{
	int n;
	char word[MAXWORD];

	while(getword(word, MAXWORD) != EOF)
		if(isalpha(word[0]))
			if((n = binsearch(word, keytab, NKEYS)) >= 0)
				keytab[n].count++;
	for(n = 0; n < NKEYS; n++)
		if(keytab[n].count > 0)
			printf("%4d %s\n", keytab[n].count, keytab[n].word);
	return 0;
}
 

/* getword: get next word or character from input */
int getword(char *word, int lim)
{
  int c,  getch(void);
  void ungetch(int);
  char *w = word;

  while (isspace(c = getch()))
	;
  if (c != EOF)
	*w++ = c;


  if (c == '"')  /* check string constants: skip until end of string */
	while ((c=getch()) != EOF && c != '"')
	  ;
  if (c == '/')			/* check for comments */
	if ((c=getch()) == '*') {
	  while ((c=getch()) != EOF && c != '*')
		;
	  if ((c=getch()) == '/')
		c = getch();
	} else
	  ungetch(*w);
  

  if (!isalpha(c)) {
	*w = '\0';
	return c;
  }
  
  /* check if variables have underscores */
  /* check for words with letters and numbers */
  for ( ; --lim > 0; w++)
	if (!isalnum(*w = getch()) && *w != '_') {
	  ungetch(*w);
	  break;
	}
  *w = '\0';
  return word[0];
}


int binsearch(char *word, struct key tab[], int n)
{
	int cond;
	int low, high, mid;

	low = 0;
	high = n - 1;
	while(low <= high) {
		mid = (low + high)/2;
		if((cond = strcmp(word, tab[mid].word)) < 0)
			high = mid - 1;
		else if(cond > 0)
			low = mid + 1;
		else
			return mid;
	}
	return -1;
}


/* getch: get a (possibly pushed back) character */
int getch(void)
{
  int c;

  if (buf != 0)
	c = buf;
  else
	c = getchar();
  buf = 0;
  return c;
}

/* ungetch: push character back on input */
void ungetch(int c)
{
  if (buf != 0)
	printf("ungetch: too many characters\n");
  else
	buf = c;
}
