#include <stdio.h>
#include <ctype.h>

int main (int argc, char *argv[])
{
  int c;
  int (*func)(int);

  if (strcmp(argv[0], "lower")  == 0) {
	func = tolower;
  } else if (strcmp(argv[0], "upper") == 0) {
	func = toupper;
  } else
	printf("error: unknown command\n");

  while((c=getchar()) != EOF) {
	putchar((*func)(c));
  }
  return 0;
}
